const GetKolkataRollItems = (
  state = [
    {
      id: "kkr1",
      quantity: 1,
      name: "French Fries",
      price: 105,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/p7g01vrjsxqgn1tm3fp6",
    },
    {
      id: "kkr2",
      quantity: 1,
      name: "Chicken Fingers",
      price: 239,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/od5hcjocgwovgjaz5npd",
    },
    {
      id: "kkr3",
      quantity: 1,
      name: "Chicken Popcorn",
      price: 209,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/atmty8cieohlrk5nvyxy",
    },
    {
      id: "kkr4",
      quantity: 1,
      name: "Jalepeno Poppers",
      price: 179,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/lno8dzsklb13hpjnq4ik",
    },
    {
      id: "kkr5",
      quantity: 1,
      name: "Veg Fingers",
      price: 165,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/od5hcjocgwovgjaz5npd",
    },
  ],
  action
) => {
  switch (action.type) {
    case "GETKOLKATAROLL":
      console.log("fetched Kolkata Roll");
      return (state = [...state]);
    default:
      return state;
  }
};

export default GetKolkataRollItems;
