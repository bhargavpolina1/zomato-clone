const GetRestuarants = (
  state = [
    {
      id: "rest1",
      name: "BurgerKing",
      description: "American, Fast Food",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/d2kya4g6zpeminutmlia",
    },
    {
      id: "rest2",
      name: "MeghanaFoods",
      description: "Biryani, Andhra",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/sotxv0gury7f7vrfvb2r",
    },
    {
      id: "rest3",
      name: "KolkataRoll",
      description: "Snacks, Beverages",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ug9l24encjpduyjxifeg",
    },
    {
      id: "rest4",
      name: "ParadiseBiryani",
      description: "Biryani, Kebabs",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/f19zzsfjldeclxpnihov",
    },
    {
      id: "rest5",
      name: "SavouryHotel",
      description: "Biryani, Mughlai",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ntgstaclhy1cpqatc6mi",
    },
  ],
  action
) => {
  switch (action.type) {
    case "FETCHRESTURANTS":
      console.log("fetched resturants");
      return (state = [...state]);
    default:
      return state;
  }
};

export default GetRestuarants;
