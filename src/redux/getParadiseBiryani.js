const GetParadiseItems = (
  state = [
    {
      id: "pb1",
      quantity: 1,
      name: "Chicken Maharaja",
      price: 295,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/qbvnd4eezzybx9dj38xv",
    },
    {
      id: "pb2",
      quantity: 1,
      name: "Chicken Rayalaseema",
      price: 285,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/amlh8ivjrwgcx4lxrki8",
    },
    {
      id: "pb3",
      quantity: 1,
      name: "Apollo Fish",
      price: 310,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ywwqavlqhhhwrpzu3nle",
    },
    {
      id: "pb4",
      quantity: 1,
      name: "Chicken Boneless Curry",
      price: 285,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ybxkminvzggpmjfbecu8",
    },
    {
      id: "pb5",
      quantity: 1,
      name: "Chicken Biryani",
      price: 280,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/zbrpxvywfsrrb7os11jf",
    },
  ],
  action
) => {
  switch (action.type) {
    case "GETPARADISE":
      console.log("fetched Paradise");
      return (state = [...state]);
    default:
      return state;
  }
};

export default GetParadiseItems;
