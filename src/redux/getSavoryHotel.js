const GetSavouryItems = (
  state = [
    {
      id: "sh1",
      quantity: 1,
      name: "Tawa Grilled Fish (Seer)",
      price: "Rs.420",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/n2oysoqpctq3gl62hija",
    },
    {
      id: "sh2",
      quantity: 1,
      name: "Chicken Kolhapuri (Spicy)",
      price: "Rs.300",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/rpccehialsva6okivl01",
    },
    {
      id: "sh3",
      quantity: 1,
      name: "Keema Roll (Kp)",
      price: "Rs.190",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/agffpaolde1kt3slym0v",
    },
    {
      id: "sh4",
      quantity: 1,
      name: "Mint Lime Juice",
      price: "Rs.65",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/nqrplmuisfnob0ljnfez",
    },
    {
      id: "sh5",
      quantity: 1,
      name: "Chicken 65 (Boneless)",
      price: "Rs.360",
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/zbrpxvywfsrrb7os11jf",
    },
  ],
  action
) => {
  switch (action.type) {
    case "GETSAVOURY":
      console.log("fetched Paradise");
      return (state = [...state]);
    default:
      return state;
  }
};

export default GetSavouryItems;
