const AddandRemoveProducts = (state = [], action) => {
  const givenData = [
    {
      id: "bgk1",
      name: "Veggie Strips - 5 Pcs",
      price: 49,
      isAddedToCart: false,
      quantity: 1,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/aodzevst0hqvlrgqumif",
    },
    {
      id: "bgk2",
      name: "King Fries",
      price: 109,
      quantity: 1,
      isAddedToCart: false,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/cvd8m9tfxadrmhd54w52",
    },
    {
      id: "bgk3",
      name: "Cheesy Fries",
      price: 109,
      quantity: 1,
      isAddedToCart: false,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/owfixesuqjm7uzxhzhrv",
    },
    {
      id: "bgk4",
      name: "Boneless Wings Large",
      price: 249,
      isAddedToCart: false,
      quantity: 1,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/qefyb4gxjhh0i9iz6ozu",
    },
    {
      id: "bgk5",
      name: "Easy Cheesy Dip",
      price: 249,
      isAddedToCart: false,
      quantity: 1,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/cfxufomgselawz85bhbo",
    },
    {
      id: "mgn1",
      quantity: 1,
      name: "Panner 65",
      price: 150,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/druwjzmfmz7qvepq3bkr",
    },
    {
      id: "mgn2",
      quantity: 1,
      name: "Chilli Paneer",
      price: 275,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/byonwwb8mzxyqluxlqpq",
    },
    {
      id: "mgn3",
      quantity: 1,
      name: "Golden Baby Corn",
      price: 270,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ocgjpnijydsughgjnkk0",
    },
    {
      id: "mgn4",
      quantity: 1,
      name: "Veg Manchurian",
      price: 260,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/etnsehnoylzjgipeapde",
    },
    {
      id: "mgn5",
      quantity: 1,
      name: "Chilly Chicken",
      price: 285,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ry3c3f518z10t4olu4l7",
    },
    {
      id: "kkr1",
      quantity: 1,
      name: "French Fries",
      price: 105,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/p7g01vrjsxqgn1tm3fp6",
    },
    {
      id: "kkr2",
      quantity: 1,
      name: "Chicken Fingers",
      price: 239,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/od5hcjocgwovgjaz5npd",
    },
    {
      id: "kkr3",
      quantity: 1,
      name: "Chicken Popcorn",
      price: 209,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/atmty8cieohlrk5nvyxy",
    },
    {
      id: "kkr4",
      quantity: 1,
      name: "Jalepeno Poppers",
      price: 179,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/lno8dzsklb13hpjnq4ik",
    },
    {
      id: "kkr5",
      quantity: 1,
      name: "Veg Fingers",
      price: 165,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/od5hcjocgwovgjaz5npd",
    },
    {
      id: "pb1",
      quantity: 1,
      name: "Chicken Maharaja",
      price: 285,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/qbvnd4eezzybx9dj38xv",
    },
    {
      id: "pb2",
      quantity: 1,
      name: "Chicken Rayalaseema",
      price: 285,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/amlh8ivjrwgcx4lxrki8",
    },
    {
      id: "pb3",
      quantity: 1,
      name: "Apollo Fish",
      price: 310,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ywwqavlqhhhwrpzu3nle",
    },
    {
      id: "pb4",
      quantity: 1,
      name: "Chicken Boneless Curry",
      price: 285,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ybxkminvzggpmjfbecu8",
    },
    {
      id: "pb5",
      quantity: 1,
      name: "Chicken Biryani",
      price: 280,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/zbrpxvywfsrrb7os11jf",
    },
    {
      id: "sh1",
      quantity: 1,
      name: "Tawa Grilled Fish (Seer)",
      price: 420,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/n2oysoqpctq3gl62hija",
    },
    {
      id: "sh2",
      quantity: 1,
      name: "Chicken Kolhapuri (Spicy)",
      price: 300,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/rpccehialsva6okivl01",
    },
    {
      id: "sh3",
      quantity: 1,
      name: "Keema Roll (Kp)",
      price: 190,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/agffpaolde1kt3slym0v",
    },
    {
      id: "sh4",
      quantity: 1,
      name: "Mint Lime Juice",
      price: 65,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/nqrplmuisfnob0ljnfez",
    },
    {
      id: "sh5",
      quantity: 1,
      name: "Chicken 65 (Boneless)",
      price: 360,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/zbrpxvywfsrrb7os11jf",
    },
  ];
  switch (action.type) {
    case "ADDTOCART":
      console.log("Added item to cart");
      console.log(action.payload);

      if (state.length === 0) {
        const filteredData = givenData.filter((eachItem) => {
          return eachItem.id === action.payload.id;
        });

        state = [...state, ...filteredData];
      } else {
        const IdsInCart = state.map((eachItemInCart) => eachItemInCart.id);

        if (IdsInCart.includes(action.payload.id)) {
          const requiredObj = state.map((eachItem) => {
            if (eachItem.id === action.payload.id) {
              eachItem.quantity += 1;
              return eachItem;
            }
            return eachItem;
          });

          state = [...requiredObj];
        }else{
          const filteredData = givenData.filter((eachItem) => {
            return eachItem.id === action.payload.id;
          });
          state = [...state, ...filteredData]

        }
      }
      return state;

    case "REMOVEFROMCART":
      console.log("Remove item from cart:", action.payload.id);

      const filteredState = state.filter((obj) => obj.id !== action.payload.id);

      return (state = filteredState);

    case "IncreaseQuantity":
      const incId = action.payload.id;
      const updatedState = state.map((eachProduct) => {
        if (eachProduct.id === incId) {
          eachProduct.quantity += 1;
          return eachProduct;
        } else {
          return eachProduct;
        }
      });
      return (state = updatedState);

    case "DecreaseQuantity":
      const decId = action.payload.id;
      const removeProduct = state.reduce((accumilator,eachProduct) => {
          if(eachProduct.id === decId){
            if (eachProduct.quantity > 1) {
              eachProduct.quantity -= 1;
              accumilator.push(eachProduct)
            }
          }
          else{
            accumilator.push(eachProduct)
          }
          return accumilator
      },[])
      console.log(removeProduct)
      return (state = removeProduct);

    case "EMPTYTHECART":
      return (state = []);
    default:
      return state;
  }
};

export default AddandRemoveProducts;
