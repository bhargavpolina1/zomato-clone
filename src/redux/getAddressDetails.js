import validator from "validator";

const GetAddressDetails = (
  state = {
    name: "",
    mobileNumber: "",
    FlatNo: "",
    streetName: "",
    landMark: "",
    city: "",
    stateName: "",
    countryName: "",
    pincode: "",
    nameErrorMessage: "",
    mobileErrorMessage: "",
    flatNoErrorMessage: "",
    streetNameErrorMessage: "",
    cityErrorMessage: "",
    stateErrorMessage: "",
    countryErrorMessage: "",
    pinCodeErrorMessage: "",
    isNameValid: false,
    isMobileNumberValid: false,
    isFlatNoValid: false,
    isStreetValid: false,
    isCityValid: false,
    isStateValid: false,
    isCountryValid: false,
    isPinValid: false,
    isFormValidationSucessful: false,
  },
  action
) => {
  switch (action.type) {
    case "OnNameChange":
      const enteredName = action.payload.value;
      const checkValidName = validator.isAlpha(enteredName);
      console.log(checkValidName);

      if (checkValidName) {
        state = {
          ...state,
          name: enteredName,
          nameErrorMessage: "",
          isNameValid: true,
        };
      } else {
        state = {
          ...state,
          nameErrorMessage: "*Invalid name. It should only have alphabets",
          isNameValid: false,
          name: enteredName,
        };
      }
      return state;
    case "OnMobileNumberChange":
      const enteredMobileNumber = action.payload.value;
      const checkValidMobileNumber = validator.isMobilePhone(
        enteredMobileNumber,
        ["en-IN"]
      );
      console.log(checkValidMobileNumber);
      if (checkValidMobileNumber) {
        state = {
          ...state,
          mobileNumber: enteredMobileNumber,
          mobileErrorMessage: "",
          isMobileNumberValid: true,
        };
      } else {
        state = {
          ...state,
          mobileErrorMessage:
            "*Invalid mobile number. It should have 10 digits",
          isMobileNumberValid: false,
          mobileNumber: enteredMobileNumber,
        };
      }

      return state;

    case "onFlatNoChange":
      const enteredFlatNo = action.payload.value;
      const checkValidFlatNo = validator.isEmpty(enteredFlatNo, {
        ignore_whitespace: true,
      });
      console.log(checkValidFlatNo);

      if (!checkValidFlatNo) {
        state = {
          ...state,
          FlatNo: enteredFlatNo,
          isFlatNoValid: true,
          flatNoErrorMessage: "",
        };
      } else {
        state = {
          ...state,
          FlatNo: enteredFlatNo,
          isFlatNoValid: false,
          flatNoErrorMessage: "*Flat no should not be empty",
        };
      }
      return state;
    case "onStreetChange":
      const enteredStreetNo = action.payload.value;
      const isStreetNameValid = validator.isEmpty(enteredStreetNo, {
        ignore_whitespace: true,
      });
      console.log(isStreetNameValid);

      if (!isStreetNameValid) {
        state = {
          ...state,
          streetName: enteredStreetNo,
          isStreetValid: true,
          streetNameErrorMessage: "",
        };
      } else {
        state = {
          ...state,
          streetName: enteredStreetNo,
          isStreetValid: false,
          streetNameErrorMessage: "*Street name should not be empty",
        };
      }
      return state;
    case "onLandMarkChange":
      state = { ...state, landMark: action.payload.value };
      return state;
    case "onCityChange":
      const enteredCity = action.payload.value;
      const isCityNameValid = validator.isAlpha(enteredCity);

      if (isCityNameValid) {
        state = {
          ...state,
          city: enteredCity,
          isCityValid: true,
          cityErrorMessage: "",
        };
      } else {
        state = {
          ...state,
          city: enteredCity,
          isCityValid: false,
          cityErrorMessage:
            "*Enter a valid city. It should only have alphabets",
        };
      }

      return state;
    case "onStateChange":
      const enteredState = action.payload.value;
      const isStateNameValid = validator.isAlpha(enteredState);
      if (isStateNameValid) {
        state = {
          ...state,
          stateName: enteredState,
          isStateValid: true,
          stateErrorMessage: "",
        };
      } else {
        state = {
          ...state,
          stateName: enteredState,
          isStateValid: false,
          stateErrorMessage:
            "*Enter a valid state. It should only have alphabets",
        };
      }
      return state;
    case "onCountryChange":
      const enteredCountry = action.payload.value;
      const isCountryNameValid = validator.isAlpha(enteredCountry);

      if (isCountryNameValid) {
        state = {
          ...state,
          countryName: enteredCountry,
          isCountryValid: true,
          countryErrorMessage: "",
        };
      } else {
        state = {
          ...state,
          countryName: enteredCountry,
          isCountryValid: false,
          countryErrorMessage:
            "*Enter a valid country. It should only have alphabets",
        };
      }

      return state;
    case "onPinCodeChange":
      const enteredPincode = action.payload.value;
      const isPincodeValid = validator.isPostalCode(enteredPincode, "IN");
      console.log(isPincodeValid);

      if (isPincodeValid) {
        state = {
          ...state,
          pincode: enteredPincode,
          isPinValid: true,
          pinCodeErrorMessage: "",
        };
      } else {
        state = {
          ...state,
          pincode: enteredPincode,
          isPinValid: false,
          pinCodeErrorMessage: "*Invalid pincode. It should contain 6 digits",
        };
      }
      return state;
    case "FORMVALIDATIONFAILED":
      if (
        state.isNameValid &&
        state.isMobileNumberValid &&
        state.isFlatNoValid &&
        state.isStreetValid &&
        state.isCityValid &&
        state.isStateValid &&
        state.isCountryValid &&
        state.isPinValid
      ) {
        state = {
          ...state,
          nameErrorMessage: "",
          mobileErrorMessage: "",
          flatNoErrorMessage: "",
          streetNameErrorMessage: "",
          cityErrorMessage: "",
          stateErrorMessage: "",
          countryErrorMessage: "",
          pinCodeErrorMessage: "",
          isFormValidationSucessful: true,
        };
      } else {
        if (!state.isNameValid) {
          state = {
            ...state,
            nameErrorMessage: "*Invalid name. It should only have alphabets",
          };
        } else {
          state = {
            ...state,
            nameErrorMessage: "",
          };
        }

        if (!state.isMobileNumberValid) {
          state = {
            ...state,
            mobileErrorMessage:
              "*Invalid mobile number. It should have 10 digits",
          };
        } else {
          state = {
            ...state,
            mobileErrorMessage: "",
          };
        }
        if (!state.isFlatNoValid) {
          state = {
            ...state,
            flatNoErrorMessage: "*Flat no should not be empty",
          };
        } else {
          state = {
            ...state,
            flatNoErrorMessage: "",
          };
        }
        if (!state.isStreetValid) {
          state = {
            ...state,
            streetNameErrorMessage: "*Street name should not be empty",
          };
        } else {
          state = {
            ...state,
            streetNameErrorMessage: "",
          };
        }
        if (!state.isCityValid) {
          state = {
            ...state,
            cityErrorMessage:
              "*Enter a valid city. It should only have alphabets",
          };
        } else {
          state = {
            ...state,
            cityErrorMessage: "",
          };
        }
        if (!state.isStateValid) {
          state = {
            ...state,
            stateErrorMessage:
              "*Enter a valid state. It should only have alphabets",
          };
        } else {
          state = {
            ...state,
            stateErrorMessage: "",
          };
        }
        if (!state.isCountryValid) {
          state = {
            ...state,
            countryErrorMessage:
              "*Enter a valid country. It should only have alphabets",
          };
        } else {
          state = {
            ...state,
            countryErrorMessage: "",
          };
        }
        if (!state.isPinValid) {
          state = {
            ...state,
            pinCodeErrorMessage: "*Invalid pincode. It should contain 6 digits",
          };
        } else {
          state = {
            ...state,
            pinCodeErrorMessage: "*",
          };
        }
        /* state = {
          ...state,
          nameErrorMessage: "*Invalid name. It should only have alphabets",
          mobileErrorMessage:
            "*Invalid mobile number. It should have 10 digits",
          flatNoErrorMessage: "*Flat no should not be empty",
          streetNameErrorMessage: "*Street name should not be empty",
          cityErrorMessage:
            "*Enter a valid city. It should only have alphabets",
          stateErrorMessage:
            "*Enter a valid state. It should only have alphabets",
          countryErrorMessage:
            "*Enter a valid country. It should only have alphabets",
          pinCodeErrorMessage: "*Invalid pincode. It should contain 6 digits",
          isFormValidationSucessful:false,
        }; */
      }
      return state;
    default:
      return state;
  }
};

export default GetAddressDetails;
