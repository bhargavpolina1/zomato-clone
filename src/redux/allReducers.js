import GetProducts from "./reducer";
import GetRestuarants from "./getResturants";
import AddandRemoveProducts from "./addToCartReducer";
import GetMeghanaFoodsItems from "./getMeghanaFoods";
import GetBurgerKingItems from "./getBurgerKing";
import GetKolkataRollItems from "./getKolkataRoll";
import GetParadiseItems from "./getParadiseBiryani";
import GetSavouryItems from "./getSavoryHotel";
import GetAddressDetails from "./getAddressDetails";
import { combineReducers } from "redux";



const AllReducers = combineReducers({
    GetProducts,
    GetRestuarants,
    AddandRemoveProducts,
    GetMeghanaFoodsItems,
    GetBurgerKingItems,
    GetKolkataRollItems,
    GetParadiseItems,
    GetSavouryItems,
    GetAddressDetails
})



export default AllReducers;
