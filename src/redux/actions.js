export const fetchData = () => (dispatch) => {
  dispatch({
    type: "FETCHDATA",
  });
};

export const fetchresturants = () => (dispatch) => {
  dispatch({
    type: "FETCHRESTURANTS",
  });
};

export const getburgerking = () => (dispatch) => {
  dispatch({
    type: "GETBURGERKING",
  });
};

export const getMeghanaFoods = (event) => (dispatch) => {
  dispatch({
    type: "GETMEGHANAFOODS",
  });
};

export const getKolkataRoll = () => (dispatch) => {
  dispatch({
    type: "GETKOLKATAROLL",
  });
};
export const getParadise = () => (dispatch) => {
  dispatch({
    type: "GETPARADISE",
  });
};
export const getSavoury = () => (dispatch) => {
  dispatch({
    type: "GETSAVOURY",
  });
};
export const addToCart = (event) => (dispatch) => {
  console.log(event);
  dispatch({
    type: "ADDTOCART",
    payload: {
      id: event.target.id,
    },
  });
};

export const removeFromCart = (event) => (dispatch) => {
  console.log("send id: ", event.target.id);

  dispatch({
    type: "REMOVEFROMCART",
    payload: {
      id: event.target.id,
    },
  });
};

export const IncreaseQuantity = (event) => (dispatch) => {
  dispatch({
    type: "IncreaseQuantity",
    payload: {
      id: event.target.id,
    },
  });
};

export const DecreaseQuantity = (event) => (dispatch) => {
  dispatch({
    type: "DecreaseQuantity",
    payload: {
      id: event.target.id,
    },
  });
};

export const OnNameChange = (event) => (dispatch) => {
  dispatch({
    type: "OnNameChange",
    payload: {
      value: event.target.value,
    },
  });
};

export const OnMobileNumberChange = (event) => (dispatch) => {
  dispatch({
    type: "OnMobileNumberChange",
    payload: {
      value: event.target.value,
    },
  });
};

export const onFlatNoChange = (event) => (dispatch) => {
  dispatch({
    type: "onFlatNoChange",
    payload: {
      value: event.target.value,
    },
  });
};
export const onStreetChange = (event) => (dispatch) => {
  dispatch({
    type: "onStreetChange",
    payload: {
      value: event.target.value,
    },
  });
};

export const onLandMarkChange = (event) => (dispatch) => {
  dispatch({
    type: "onLandMarkChange",
    payload: {
      value: event.target.value,
    },
  });
};

export const onCityChange = (event) => (dispatch) => {
  dispatch({
    type: "onCityChange",
    payload: {
      value: event.target.value,
    },
  });
};

export const onStateChange = (event) => (dispatch) => {
  dispatch({
    type: "onStateChange",
    payload: {
      value: event.target.value,
    },
  });
};

export const onCountryChange = (event) => (dispatch) => {
  dispatch({
    type: "onCountryChange",
    payload: {
      value: event.target.value,
    },
  });
};

export const onPinCodeChange = (event) => (dispatch) => {
  dispatch({
    type: "onPinCodeChange",
    payload: {
      value: event.target.value,
    },
  });
};

export const emptyTheCart = () => (dispatch) => {
  dispatch({
    type: "EMPTYTHECART",
  });
};

export const formValidationFailed = () => (dispatch) => {
  dispatch({
    type: "FORMVALIDATIONFAILED",
  });
};

export const removeIfZero = () => (dispatch) => {
  dispatch({
    type: "REMOVEIFZERO",
  });
};
