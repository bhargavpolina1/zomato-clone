const GetMeghanaFoodsItems = (
  state = [
    {
      id: "mgn1",
      quantity: 1,
      name: "Panner 65",
      price: 150,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/druwjzmfmz7qvepq3bkr",
    },
    {
      id: "mgn2",
      quantity: 1,
      name: "Chilli Paneer",
      price: 275,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/byonwwb8mzxyqluxlqpq",
    },
    {
      id: "mgn3",
      quantity: 1,
      name: "Golden Baby Corn",
      price: 270,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ocgjpnijydsughgjnkk0",
    },
    {
      id: "mgn4",
      quantity: 1,
      name: "Veg Manchurian",
      price: 260,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/etnsehnoylzjgipeapde",
    },
    {
      id: "mgn5",
      quantity: 1,
      name: "Chilly Chicken",
      price: 285,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ry3c3f518z10t4olu4l7",
    },
  ],
  action
) => {
  switch (action.type) {
    case "GETMEGHANAFOODS":
      console.log("fetched Meghana Foods");
      return (state = [...state]);
    default:
      return state;
  }
};

export default GetMeghanaFoodsItems;
