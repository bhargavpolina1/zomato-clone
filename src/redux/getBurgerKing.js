const GetBurgerKingItems = (
  state = [
    {
      id: "bgk1",
      name: "Veggie Strips - 5 Pcs",
      price: 49,
      quantity:1,
      isAddedToCart:false,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/aodzevst0hqvlrgqumif",
    },
    {
      id: "bgk2",
      name: "King Fries",
      price:109,
      isAddedToCart:false,
      quantity:1,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/cvd8m9tfxadrmhd54w52",
    },
    {
      id: "bgk3",
      name: "Cheesy Fries",
      price:109,
      quantity:1,
      isAddedToCart:false,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/owfixesuqjm7uzxhzhrv",
    },
    {
      id: "bgk4",
      name: "Boneless Wings Large",
      price:249,
      quantity:1,
      isClicked:false,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/qefyb4gxjhh0i9iz6ozu",
    },
    {
      id: "bgk5",
      name: "Easy Cheesy Dip",
      price:249,
      quantity:1,
      isAddedToCart:false,
      coverImage:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/cfxufomgselawz85bhbo",
    },
  ],
  action
) => {
  switch (action.type) {
    case "GETBURGERKING":
      console.log("fetched Burger King");
      return (state = [...state]);
    case 'isSelected':
      const selectedData = state.map((obj)=>{
        if(obj.id === action.payload.id){
          obj.isAddedToCart = true
          return obj
        }
        else{
          return obj
        }
      })
      return state = selectedData
    default:
      return state;
  }
};

export default GetBurgerKingItems;
