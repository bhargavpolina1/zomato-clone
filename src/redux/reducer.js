const GetProducts = (
  state = [
    {
      id: 1,
      quantity:1,
      itemName: "Panner 65",
      price: "Rs.150",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/druwjzmfmz7qvepq3bkr",
    },
    {
      id: 2,
      quantity:1,
      itemName: "Chilli Paneer",
      price: "Rs.275",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/byonwwb8mzxyqluxlqpq",
    },
    {
      id: 3,
      quantity:1,
      itemName: "Golden Baby Corn",
      price: "Rs.270",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ocgjpnijydsughgjnkk0",
    },
    {
      id: 4,
      quantity:1,
      itemName: "Veg Manchurian",
      price: "Rs.260",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/etnsehnoylzjgipeapde",
    },
    { id:5,
      quantity:1,
      itemName: "Chilly Chicken",
      price: "Rs.285",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ry3c3f518z10t4olu4l7",
    },
    { id:6,
      quantity:1,
      itemName: "Chicken Maharaja",
      price: "Rs.285",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/qbvnd4eezzybx9dj38xv",
    },
    { id:7,
      quantity:1,
      itemName: "Chicken Rayalaseema",
      price: "Rs.285",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/amlh8ivjrwgcx4lxrki8",
    },
    { id:8,
      quantity:1,
      itemName: "Apollo Fish",
      price: "Rs.310",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ywwqavlqhhhwrpzu3nle",
    },
    { id:9,
      quantity:1,
      itemName: "Chicken Boneless Curry",
      price: "Rs.285",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ybxkminvzggpmjfbecu8",
    },
    { id:10,
      quantity:1,
      itemName: "Chicken Biryani",
      price: "Rs.280",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/zbrpxvywfsrrb7os11jf",
    },
    { id:11,
      quantity:1,
      itemName: "Chicken Boneless Biryani",
      price: "Rs.295",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/iivuhjc2mswi9lublktf",
    },
    { id:12,
      quantity:1,
      itemName: "Lollipop Biryani",
      price: "Rs.290",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/tscwxqko4pabme8gj2ty",
    },
    { id:13,
      quantity:1,
      itemName: "Prawns Biryani",
      price: "Rs.365",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/pxqhnjvdhm3nyqkhxute",
    },
    { id:14,
      quantity:1,
      itemName: "Palak Paneer Curry",
      price: "Rs.250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/slaicd3fv71xcsxblb2w",
    },
    { id:15,
      quantity:1,
      itemName: "Egg Biryani",
      price: "Rs.260",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/wcvun7u8l0jzkzcf8zap",
    },
    { id:16,
      quantity:1,
      itemName: "Mushroom Biryani",
      price: "Rs.250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ujngpmxwklmqpruqhx9h",
    },
    { id:17,
      quantity:1,
      itemName: "Dal Fry",
      price: "Rs.260",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/czmhxhzvc8lqklawenrv",
    },
    { id:18,
      quantity:1,
      itemName: "Paneer Butter Masala",
      price: "Rs.280",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ksjxi61s8xqlj8cqp4zs",
    },
    { id:19,
      quantity:1,
      itemName: "Spl Veg Biryani",
      price: "Rs.260",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/fsitbray4gq1kxcndqqx",
    },
    { id:20,
      quantity:1,
      itemName: "Aloo Dum Biryani",
      price: "Rs.245",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/mtuzvgdmipuikj6pxllr",
    },
  ],
  action
) => {
  switch (action.type) {
    case "FETCHDATA":
      console.log("fetched data");
      return (state = [...state]);
    default:
      return state;
  }
};

export default GetProducts;