import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  removeFromCart,
  IncreaseQuantity,
  DecreaseQuantity,
} from "../redux/actions";
import "./addToCart.css";

class AddToCart extends Component {

  handleEvent = (event) => {
    console.log('removed id:', event.target.id)
    this.props.removeFromCart(event)
  }
  render() {
    if (this.props.ItemsInCartData.length === 0) {
      return (
        <div className="itemsTopContainer">
          <h1>Cart is Empty </h1>
          <Link to="/resturants">
            <button className="btn btn-danger text-white">Add Items</button>
          </Link>
        </div>
      );
    } else {
      return (
        <div className="container-fluid itemsTopContainer">
          <Link to="/resturants" className="text-decoration-none">
            <button className="btn btn-danger d-flex align-self-start">
              Add More Items
            </button>
          </Link>
          <h1>Cart</h1>
          <div className="row d-flex justify-content-center">
            {this.props.ItemsInCartData.map((eachItem) => {
              console.log(eachItem)
              return (
                <div
                  className="col-12 d-flex mb-1 card-cart-container"
                  key={eachItem.id}
                >
                  <div
                    className="d-flex justify-content-evenly align-items-center"
                    style={{ width: "100%" }}
                  >
                    <div style={{ width: "30%", marginBottom: "0" }}>
                      <h6>{eachItem.name}</h6>
                      <p>Rs.{eachItem.price}</p>
                    </div>

                    <div>
                      <button
                        type="button"
                        className="btn btn-danger"
                        data-bs-toggle="modal"
                        data-bs-target={`#${eachItem.id}`}
                      >
                        Remove
                      </button>

                      <div
                        className="modal fade"
                        id={eachItem.id}
                        tabIndex="-1"
                        aria-labelledby="exampleModalLabel"
                        aria-hidden="true"
                        data-bs-backdrop="false"

                      >
                        <div className="modal-dialog modal-dialog-centered">
                          <div className="modal-content">
                            <div className="modal-header">
                              <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                              ></button>
                            </div>
                            <div className="modal-body">
                              Do you want to remove <b>{eachItem.name}</b> from the cart?
                            </div>
                            <div className="modal-footer">
                              <button
                                type="button"
                                className="btn btn-danger"
                                data-bs-dismiss="modal"
                              >
                                Close
                              </button>
                              <div>
                                <button onClick={this.handleEvent} id={eachItem.id}
                                  className="btn btn-danger text-light m-1"
                                  style={{ width: "100%" }}
                                >
                                  Remove
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div>{console.log(parseInt(eachItem.price))}</div>
                    <div
                      style={{ width: "30%" }}
                      className="d-flex flex-column align-items-center justify-content-center"
                    >
                      <div
                        style={{
                          borderRadius: "10px",
                          borderWidth: "1px",
                          borderStyle: "solid",
                          borderColor: "#d9534f",
                          width: "50%",
                        }}
                        className="d-flex justify-content-evenly align-self-center m-3"
                      >
                        <span
                          id={eachItem.id}
                          onClick={this.props.DecreaseQuantity}
                          className="changeQuantity"
                        >
                          -
                        </span>
                        <span> {eachItem.quantity}</span>
                        <span
                          id={eachItem.id}
                          onClick={this.props.IncreaseQuantity}
                          className="changeQuantity"
                        >
                          +
                        </span>
                      </div>
                      <p>
                        Rs.{" "}
                        {parseInt(eachItem.quantity) * parseInt(eachItem.price)}
                      </p>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
          <h4>
            Total: Rs.{" "}
            {this.props.ItemsInCartData.reduce((cartTotal, eachItem) => {
              cartTotal +=
                parseInt(eachItem.price) * parseInt(eachItem.quantity);
              return cartTotal;
            }, 0)}
          </h4>
          <Link to="/cart/ItemsSummary">
            <button className="btn btn-danger w-50 mb-3">Checkout</button>
          </Link>
        </div>
      );
    }
  }
}
const mapStateToProps = (state) => ({
  ItemsInCartData: state.AddandRemoveProducts,
  allItems: state.GetProducts,
});

export default connect(mapStateToProps, {
  removeFromCart,
  IncreaseQuantity,
  DecreaseQuantity,
})(AddToCart);
