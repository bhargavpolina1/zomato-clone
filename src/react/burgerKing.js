import { Component } from "react";
import { connect } from "react-redux";
import Footer from "./footer";
import "./orderOnline.css";
import { Link } from "react-router-dom";
import { addToCart } from "../redux/actions";
import { IncreaseQuantity } from "../redux/actions";
import { DecreaseQuantity } from "../redux/actions";

class BurgerKing extends Component {
  getQuantityOfItem = (itemId) => {
    console.log(`itemId is ${itemId}`);
    const quantityincart = this.props.itemsInCart.filter((object) => {
      return object.id === itemId;
    });

    if (quantityincart.length) {
      console.log(quantityincart);
      return quantityincart[0].quantity;
    }
  };
  render() {
    return (
      <div className="container itemsTopContainer">
        <Link to="/resturants" className="text-decoration-none">
          <button className="btn btn-danger d-flex align-self-start">
            Back
          </button>
        </Link>
        <h1>Burger King</h1>
        <div className="row">
          <div className="d-flex flex-wrap justify-content-center">
            {this.props.local_varible.map((eachItem) => {
              console.log(eachItem);
              return (
                <div
                  className="col-12 col-md-4 col-lg-3 m-2 d-flex flex-column justify-content-center align-items-center p-2 eachCard"
                  key={eachItem.id}
                >
                  <div className="p-1 w-100">
                    <img
                      className="w-100 rounded-3"
                      src={eachItem.coverImage}
                      alt="item"
                    />
                  </div>
                  <h4>{eachItem.name}</h4>
                  <p>Rs. {eachItem.price}</p>
                  {this.getQuantityOfItem(eachItem.id) > 0 ? (
                    <div
                      style={{
                        borderRadius: "10px",
                        borderWidth: "2px",
                        borderStyle: "solid",
                        borderColor: "#d9534f",
                        width: "50%",
                        height:"50%"
                      }}
                      className="d-flex justify-content-evenly align-items-center align-self-center"
                    >
                      {console.log(this.getQuantityOfItem(eachItem.id))}
                      <span
                        id={eachItem.id}
                        onClick={this.props.DecreaseQuantity}
                        className="changeQuantity"
                      >
                        -
                      </span>
                      <span> {this.getQuantityOfItem(eachItem.id)}</span>

                      <span
                        id={eachItem.id}
                        onClick={this.props.IncreaseQuantity}
                        className="changeQuantity"
                      >
                        +
                      </span>
                    </div>
                  ) : (
                    <button
                      style={{
                        width: "50%",
                      }}
                      id={eachItem.id}
                      name={eachItem.name}
                      price={eachItem.price}
                      quantity={eachItem.quantity}
                      className="btn btn-danger text-light"
                      onClick={this.props.addToCart}
                    >
                      Add to Cart
                    </button>
                  )}
                </div>
              );
            })}
          </div>
        </div>
        <Footer className = "w-100"/>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  local_varible: state.GetBurgerKingItems,
  itemsInCart: state.AddandRemoveProducts,
});
export default connect(mapStateToProps, {
  addToCart,
  IncreaseQuantity,
  DecreaseQuantity,
})(BurgerKing);
