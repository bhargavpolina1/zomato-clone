import { Component } from "react";
import { connect } from "react-redux";
import "./orderOnline.css";
import { Link } from "react-router-dom";
import { addToCart } from "../redux/actions";

class KolkataRoll extends Component {
  render() {
    return (
      <div className="container itemsTopContainer">
        <Link to="/resturants" className="text-decoration-none">
          <button className="btn btn-danger d-flex align-self-start">
            Back
          </button>
        </Link>
        <h1>Kolkata Roll</h1>
        <div className="row">
          <div className="d-flex flex-wrap justify-content-center">
            {this.props.local_varible.map((eachItem) => {
              return (
                <div
                  className="col-12 col-md-4 col-lg-3 m-2 d-flex flex-column justify-content-center align-items-center p-2 eachCard"
                  key={eachItem.id}
                >
                  <div className="p-1 w-100">
                    <img
                      className="w-100 rounded-3"
                      src={eachItem.coverImage}
                      alt="item"
                    />
                  </div>
                  <h4>{eachItem.name}</h4>
                  <p>Rs. {eachItem.price}</p>
                  <button
                    id={eachItem.id}
                    name={eachItem.name}
                    price={eachItem.price}
                    quantity={eachItem.quantity}
                    className="btn btn-danger text-light"
                    onClick={this.props.addToCart}
                  >
                    Add to Cart
                  </button>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  local_varible: state.GetKolkataRollItems,
});
export default connect(mapStateToProps, { addToCart })(KolkataRoll);
