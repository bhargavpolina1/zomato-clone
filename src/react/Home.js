import React, { Component } from "react";
import { Link } from "react-router-dom";
import Footer from "./footer";
import "./home.css";

class Home extends Component {
  render() {
    return (
      <div>
        <div className="d-flex flex-column justify-content-center align-items-center topContainer">
          <h1 className="text-white bg-danger p-2">Zomato</h1>
          <p className="text-light">
            Get delicious food delivered to your door step
          </p>
          <Link to="/resturants">
            <button className="btn btn-light text-danger">Order Now</button>
          </Link>
        </div>

        <Footer />
      </div>
    );
  }
}

export default Home;
