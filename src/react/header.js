import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "./header.css";

class Header extends Component {
  render() {
    return (
      <div className="headerContainer">
        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <Link to="/" className="navbar-brand text-danger navItemFont">
              Zomato
            </Link>
            <Link to="/cart" className="d-sm-block d-lg-none">
              <button
                type="button"
                className="btn btn-danger position-relative"
              >
                Cart
                <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                {this.props.local_varible.length}
                </span>
              </button>
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <Link to="/" className="nav-link text-danger navItemOnHover">
                  Home
                </Link>
                <Link
                  to="/resturants"
                  className="nav-link text-danger navItemOnHover"
                >
                  Restaurants
                </Link>
                <Link to="/cart" className="d-none d-lg-block">
                  <button
                    type="button"
                    className="btn btn-danger position-relative ms-100"
                  >
                    Cart
                    <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                      {this.props.local_varible.length}
                    </span>
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  local_varible: state.AddandRemoveProducts,
});
export default connect(mapStateToProps)(Header);
