import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "./addToCart.css";

class ItemsSummary extends Component {
  render() {
    return (
      <div className="container-fluid itemsTopContainer">
        <Link to="/cart" className="text-decoration-none">
          <button className="btn btn-danger d-flex align-self-start">
            Back to Cart
          </button>
        </Link>

        <h1>Items Summary</h1>
        <div className="row d-flex justify-content-center card-cart-container">
          {this.props.ItemsInCartData.map((eachItem) => {
            return (
              <div
                className="col-12 col-md-3 d-flex justify-content-around align-items-center p-1 m-1"
                key={eachItem.id}
              >
                <div>
                  <h4 className="me-3">{eachItem.name}</h4>
                  <p>Rs.{eachItem.price}</p>
                  <p>Quantity: {eachItem.quantity}</p>
                  <span>Sub Total:</span>
                  <p>
                    Rs. {parseInt(eachItem.quantity) * parseInt(eachItem.price)}
                  </p>
                </div>
              </div>
            );
          })}
        </div>
        <h4 className="mt-3">
          Total:{" "} Rs. {" "}
          {this.props.ItemsInCartData.reduce((cartTotal, eachItem) => {
            cartTotal += parseInt(eachItem.price) * parseInt(eachItem.quantity);
            return cartTotal;
          }, 0)}
        </h4>
        <Link to="/cart/AddAddress">
          <button className="btn btn-danger w-50 mb-3">Add Address</button>
        </Link>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  ItemsInCartData: state.AddandRemoveProducts,
  allItems: state.GetProducts,
});

export default connect(mapStateToProps)(ItemsSummary);
