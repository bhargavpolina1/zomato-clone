import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "./addToCart.css";
import { OnNameChange } from "../redux/actions";
import { OnMobileNumberChange } from "../redux/actions";
import { onFlatNoChange } from "../redux/actions";
import { onStreetChange } from "../redux/actions";
import { onLandMarkChange } from "../redux/actions";
import { onCityChange } from "../redux/actions";
import { onStateChange } from "../redux/actions";
import { onCountryChange } from "../redux/actions";
import { onPinCodeChange } from "../redux/actions";
import { emptyTheCart } from "../redux/actions";
import { formValidationFailed } from "../redux/actions";

import OrderSucessful from "./orderSucessful";
class AddressDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFormValid: false,
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
    if (
      this.props.addressData.isNameValid &&
      this.props.addressData.isMobileNumberValid &&
      this.props.addressData.isFlatNoValid &&
      this.props.addressData.isStreetValid &&
      this.props.addressData.isCityValid &&
      this.props.addressData.isStateValid &&
      this.props.addressData.isCountryValid &&
      this.props.addressData.isPinValid
    ) {
      this.setState({
        isFormValid: true,
      });
    } else {
      this.props.formValidationFailed();
    }
  };

  render() {
    return this.state.isFormValid ? (
      <OrderSucessful />
    ) : (
      <div className="container itemsTopContainer">
        <Link to="/cart/ItemsSummary" className="text-decoration-none">
          <button className="btn btn-danger d-flex align-self-start">
            Back to Items Summary
          </button>
        </Link>
        <h1>Add your address details below</h1>
        <div className="row d-flex flex-wrap">
          <div className="col-12 col-md-6">
            <form>
              <div className="mb-2">
                <label className="form-label">Name</label>
                <input
                  value={this.props.addressData.name}
                  onChange={this.props.OnNameChange}
                  type="text"
                  className="form-control  border-danger"
                />
                <p className="text-danger">
                  {this.props.addressData.nameErrorMessage}
                </p>
              </div>
              <div className="mb-2">
                <label className="form-label">Mobile Number</label>
                <input
                  type="text"
                  onChange={this.props.OnMobileNumberChange}
                  value={this.props.addressData.mobileNumber}
                  className="form-control  border-danger"
                />
                <p className="text-danger">
                  {this.props.addressData.mobileErrorMessage}
                </p>
              </div>
              <div className="mb-2">
                <label className="form-label">Flat No/Apartment Name</label>
                <input
                  type="text"
                  onChange={this.props.onFlatNoChange}
                  value={this.props.addressData.FlatNo}
                  className="form-control  border-danger"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                />
                <p className="text-danger">
                  {this.props.addressData.flatNoErrorMessage}
                </p>
              </div>
              <div className="mb-2">
                <label className="form-label">Street/Locality/Area</label>
                <input
                  type="text"
                  onChange={this.props.onStreetChange}
                  value={this.props.addressData.streetName}
                  className="form-control  border-danger"
                />
                <p className="text-danger">
                  {this.props.addressData.streetNameErrorMessage}
                </p>
              </div>
              <div className="mb-2">
                <label className="form-label">Landmark</label>
                <input
                  type="text"
                  onChange={this.props.onLandMarkChange}
                  value={this.props.addressData.landMark}
                  className="form-control  border-danger"
                />
              </div>
              <div className="mb-2">
                <label className="form-label">City</label>
                <input
                  type="text"
                  onChange={this.props.onCityChange}
                  value={this.props.addressData.city}
                  className="form-control  border-danger"
                />
                <p className="text-danger">
                  {this.props.addressData.cityErrorMessage}
                </p>
              </div>
              <div className="mb-2">
                <label className="form-label">State</label>
                <input
                  type="text"
                  onChange={this.props.onStateChange}
                  value={this.props.addressData.stateName}
                  className="form-control  border-danger"
                />
                <p className="text-danger">
                  {this.props.addressData.stateErrorMessage}
                </p>
              </div>
              <div className="mb-2">
                <label className="form-label">Country</label>
                <input
                  type="text"
                  onChange={this.props.onCountryChange}
                  value={this.props.addressData.countryName}
                  className="form-control  border-danger"
                />
                <p className="text-danger">
                  {this.props.addressData.countryErrorMessage}
                </p>
              </div>
              <div className="mb-2">
                <label className="form-label">Pincode</label>
                <input
                  type="text"
                  onChange={this.props.onPinCodeChange}
                  value={this.props.addressData.pincode}
                  className="form-control  border-danger"
                />
                <p className="text-danger">
                  {this.props.addressData.pinCodeErrorMessage}
                </p>
              </div>
              <button
                type="submit"
                onClick={this.handleSubmit}
                className="btn btn-danger mb-3"
              >
                Place Order
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  addressData: state.GetAddressDetails,
});
export default connect(mapStateToProps, {
  OnNameChange,
  OnMobileNumberChange,
  onFlatNoChange,
  onStreetChange,
  onLandMarkChange,
  onCityChange,
  onStateChange,
  onCountryChange,
  onPinCodeChange,
  emptyTheCart,
  formValidationFailed,
})(AddressDetails);
