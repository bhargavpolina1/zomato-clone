import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getburgerking } from "../redux/actions";
import "./resturants.css";
import Footer from "./footer";

class Resturants extends Component {
  render() {
    return (
      <div className="container-fluid d-flex flex-column justify-content-center align-items-center itemsTopContainer">
        <h1>Browse Menu by the Restaurant</h1>
        <div className="row d-flex w-100 justify-content-center align-items-center">
          {this.props.resturantDetails.map((eachResturant) => {
            console.log(this.props.resturantDetails);
            return (
              <div
                className="col-12 col-md-3 m-3 p-2 eachCard"
                key={eachResturant.id}
              >
                <div>
                  <img
                    className="w-100 rounded-3"
                    src={eachResturant.coverImage}
                    alt="cover pic"
                  ></img>
                </div>
                <div>
                  <h4>{eachResturant.name}</h4>
                  <p>{eachResturant.description}</p>

                  <button
                    className="btn btn-danger"
                  ><Link to={`/resturants/${eachResturant.name}`} className = "text-decoration-none text-light">
                    Browse Menu
                    </Link>
                  </button>
                </div>
              </div>
            );
          })}
        </div><Footer/>
      </div>

    );
  }
}
const mapStateToProps = (state) => ({
  resturantDetails: state.GetRestuarants,
});
export default connect(mapStateToProps, { getburgerking })(Resturants);
