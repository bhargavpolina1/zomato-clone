import React, { Component } from "react";
import { connect } from "react-redux";
import "./orderOnline.css";

export class OrderSucessful extends Component {
  render() {
    return (
      <div className="container itemsTopContainer">
        <h1>
          Dear {this.props.EnteredAddressDetails.name}, your order is placed
          sucessfully.
        </h1>
        <p>
          Your order of {this.props.DetailsOfItemsInCart.length} items will be
          delivered in 38 minutes.
        </p>
        <div className="row">
          <div className="col-12">
            <div className="eachCard p-2 d-flex justify-content-evenly">
              <div className="text-start w-50">
                <h4>Items Details</h4>
                {this.props.DetailsOfItemsInCart.map((eachItem) => {
                  return (
                    <div key={eachItem.id}>
                      <p>
                        {eachItem.name} ({eachItem.quantity})
                      </p>
                    </div>
                  );
                })}
              </div>

              <div className="text-start w-50">
                <h4>Delivery Details</h4>
                <p>
                  <b>Name: </b>
                  {this.props.EnteredAddressDetails.name}
                </p>
                <p>
                  <b>Mobile Number: </b>
                  {this.props.EnteredAddressDetails.mobileNumber}
                </p>
                <p>
                  <b>Address: </b>
                  {this.props.EnteredAddressDetails.FlatNo}, {this.props.EnteredAddressDetails.streetName}, {this.props.EnteredAddressDetails.city}, {this.props.EnteredAddressDetails.stateName}, {this.props.EnteredAddressDetails.countryName}-{this.props.EnteredAddressDetails.pincode}. Landmark: {this.props.EnteredAddressDetails.landMark}
                </p>
              </div>
            </div>
            <h1 className = "mt-5 mb-5">Thank you!!!</h1>
            <h1>See you again at another mealtime!!!</h1>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  EnteredAddressDetails: state.GetAddressDetails,
  DetailsOfItemsInCart: state.AddandRemoveProducts,
});
export default connect(mapStateToProps)(OrderSucessful);
