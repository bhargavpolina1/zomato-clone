import Home from "./react/Home";
import Header from "./react/header";
import OrderOnline from "./react/OrderOnline";
import AddToCart from "./react/addToCart";
import Resturants from "./react/resturants";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import BurgerKing from "./react/burgerKing";
import MeghanaFoods from "./react/meghanaFoods";
import KolkataRoll from "./react/kolkataRoll";
import ParadiseBiryani from "./react/paradiseBiryani";
import SavouryHotel from "./react/savouryHotel";
import ItemsSummary from "./react/itemsSummary";
import AddressDetails from "./react/addressDetails";
import OrderSucessful from "./react/orderSucessful";
import Login from "./react/login";
import "./App.css";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/orderOnline" component={OrderOnline} />
          <Route exact path="/cart" component={AddToCart} />
          <Route exact path="/resturants" component={Resturants} />
          <Route exact path="/resturants/BurgerKing" component={BurgerKing} />
          <Route
            exact
            path="/resturants/MeghanaFoods"
            component={MeghanaFoods}
          />
          <Route exact path="/resturants/KolkataRoll" component={KolkataRoll} />
          <Route
            exact
            path="/resturants/ParadiseBiryani"
            component={ParadiseBiryani}
          />
          <Route
            exact
            path="/resturants/SavouryHotel"
            component={SavouryHotel}
          />
          <Route exact path="/cart/ItemsSummary" component={ItemsSummary} />
          <Route exact path="/cart/AddAddress" component={AddressDetails} />
          <Route exact path="/cart/orderSucessful" component={OrderSucessful} />
          <Route exact path="/login" component={Login} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
export default App;
